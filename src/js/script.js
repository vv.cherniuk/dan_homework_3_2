const navbarButton = document.getElementById("navbarButton");
const navbarList = document.querySelector(".header__navbar__list");
const navbarElement = navbarButton.querySelector('i');

window.addEventListener("click", (event) => {
  if (
    event.target === navbarButton ||
    event.target === navbarButton.children[0]
  ) {
    navbarList.classList.toggle("header__navbar__list--hide");
    navbarElement.classList.toggle('fa-bars');
    navbarElement.classList.toggle('fa-xmark');
  }
  if (
    event.target !== navbarButton &&
      event.target !== navbarButton.children[0] &&
    !navbarList.classList.contains("header__navbar__list--hide")
  ) {
    navbarList.classList.add("header__navbar__list--hide");
    navbarElement.classList.toggle('fa-xmark');
    navbarElement.classList.toggle('fa-bars');
  }
});

